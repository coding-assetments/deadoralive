# DeadOrAlive

[![pipeline status](https://gitlab.com/coding-assetments/deadoralive/badges/main/pipeline.svg)](https://gitlab.com/coding-assetments/deadoralive/-/commits/main) 
[![coverage report](https://gitlab.com/coding-assetments/deadoralive/badges/main/coverage.svg)](https://gitlab.com/coding-assetments/deadoralive/-/commits/main)
[![Latest Release](https://gitlab.com/coding-assetments/deadoralive/-/badges/release.svg)](https://gitlab.com/coding-assetments/deadoralive/-/releases)

DeadOrAlive is a bacterial life simulator CLI program. It's purpose is to visualize
through a given iterations count the evolution of a bacterial group.

## Get started

### Requirements

This software needs Python 3.10 or later to run.

No third-party package is required except for running tests suite.
If you need to run it, install requirements from `requirements/testing.txt` as following:

```bash
python -m pip install -r requirements/testing.txt
```

### Usage

Clone this repository then from the project directory run:

```bash
python -m deadoralive -h
```

The software help will be displayed in the terminal.

### Examples

To run a simulation from input string, execute from the project directory:

```bash
python -m deadoralive -i '6 3 4;0 0;1 1;2 2;3 3'

# This will run a simulation on a 6x3 grid and computes the 4th iteration
# with the [0;0, 1;1, 2;2, 3;3] alive bacteria
```

To run a simulation from input file, execute from project directory:
```bash
python -m deadoralive -f data.doa

# This will run a simulation with the parameters stored in data.doa file content
```

### Data format

The data format must respect the following specification:

<pre>
grid_width grid_height iterations
bacterium_1_x bacterium_1_y
bacterium_2_x bacterium_2_y
...
bacterium_N_x bacterium_N_y
</pre>

for example:

<pre>
6 3 4
0 0
1 1
2 2
3 3
</pre>

### Display

Once you have run a simulation, the computed solution will appear to you as:

<pre>
iteration X
+-------+
|X O X O|
|X X X X|
+-------+
</pre>

for a 4x2 grid, **iteration** the computed iteration number, **X** for a dead bacterium and **O** for a living bacterium.

## Configuration

This software doesn't need specific configuration

## Contributing & bug issues

Feel free to contribute to this software by opening a new [merge requests](https://gitlab.com/coding-assetments/deadoralive/-/merge_requests/new)

If you find a bug, please report it by opening a new [issue](https://gitlab.com/coding-assetments/deadoralive/-/issues/new) using
the following template:

```md
# Title

Short description

|        OS        |Python version|
|------------------|--------------|
| GNU/Linux Fedora |    3.10.5    |

[ ] The bug is reproductible
[ ] Tests suite has been successfully run

Detailled description
```

## Documentation

The technical documentation is served by GitLab Pages at https://coding-assetments.gitlab.io/deadoralive/

## License

This software is released under [BSD Zero Clause License](https://spdx.org/licenses/0BSD.html).
