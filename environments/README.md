# Python environments

This directory is intended to groups all virtual environment that are needed during
the development phase.

As you can found a different `requirements.txt` file per environment (_development_, _testing_, _..._), you can create the corresponding virtual environment in this place to keep a clean project tree.
