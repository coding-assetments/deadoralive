# Python requirements

This directory is intended to group all Pip requirements files for each different environments.

The advantage of this is to separate needed packages and lighten network bandwith when, for example, testing suite packages will be not required for a production deployment.
