# SPDX-License-Identifier: 0BSD

"""Command-Line Interface features module"""

from __future__ import annotations

from argparse import ArgumentParser, RawTextHelpFormatter

from deadoralive.parser import Parser
from deadoralive.grid import Grid

PROGRAM_EPILOG = \
'''
data format:
Input string must be comma separated such as :
    "grid_width grid_height iteration; bacterium_N_x bacterium_N_y;...;"

File format must follow:
    grid_width grid_height iteration
    bacterium_N_x bacterium_N_y
    ...

note:
This program has been developed for coding assessments to apply at Ekinox.io
'''

def handle_cli_arguments() -> ArgumentParser:
    """Set CLI arguments then returns a parser

    Returns
    ^^^^^^^
    argparse.ArgumentParser
    """
    parser = ArgumentParser(
        prog = 'Dead Or Alive',
        description = 'CLI bacteria simulator',
        epilog = PROGRAM_EPILOG,
        formatter_class=RawTextHelpFormatter
    )

    group = parser.add_mutually_exclusive_group(
        required = True,
    )

    group.add_argument(
        '-i',
        '--input',
        type = str,
        action = 'store',
        help = 'String format data input'
    )

    group.add_argument(
        '-f',
        '--file',
        type = str,
        action = 'store',
        help = 'Path to file that contains data input'
    )

    return parser

def parse_input_data(args: dict) -> Parser:
    """Handle simulation data parsing

    Match on program arguments then call the good
    method to parse input data and returns the parser

    Parameters
    ^^^^^^^^^^
    :param args: *dict*
        Parsed CLI arguments

    Returns
    ^^^^^^^
    deadoralive.Parser
    """

    parser = Parser()

    if args.input:
        parser.parse(
            args.input.replace(';', '\n')
    )
    else:
        parser.load(args.file)

    return parser

def build_simulation_grid(parser: Parser) -> Grid:
    """Build the simulation grid, populates it with parsed data values

    Parameters
    ^^^^^^^^^^
    :param parser: *deadoralive.Parser*

    Returns
    ^^^^^^^
    deadoralive.Grid
    """

    width = parser.data['grid_width']
    height = parser.data['grid_height']
    population = parser.data['bacteria']

    grid = Grid(width, height)

    grid.populate_t0(*population)

    return grid

def run_simulation(grid: Grid, iterations: int) -> None:
    """Run the simulation then display the grid after N iterations

    Parameters
    ^^^^^^^^^^
    :param grid: *deadoralive.Grid*
        Initialized and populated simulation grid
    :param iterations: *int*
        Iterations to compute
    """

    for _ in range(iterations):
        grid.do_iteration()

    print(grid)
