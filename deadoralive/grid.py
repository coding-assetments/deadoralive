# SPDX-License-Identifier: 0BSD

"""Grid model related features module"""

from __future__ import annotations
import copy
from typing import List

from deadoralive.bacterium import Bacterium

class BadInitialGridSizeError(ValueError):
    """Bad grid scale related exception

    Inherits from builtin ValueError exception
    """

class PopulateOutOfBoundsError(ValueError):
    """Out of bounds population related exception

    Inherits from builtin ValueError exception
    """

class Grid:
    """Grid class represents simulation environment

    Grid holds simulation bacteria states at each iterations

    Attributes
    ^^^^^^^^^^
    :attr width: *int*
        Grid width
    :attr height: *int*
        Grid height
    :attr iterations: *int*
        Grid current iterations count
    :attr bacteria: *list[list]*
        Bacteria holder
    """

    @classmethod
    def evaluate_next(cls, alive_neighbourgs_count, istate = True):
        """Evaluate simulation rules for next iteration

        Parameters
        ^^^^^^^^^^
        :param alive_neighbours_count: *int*
            Count of alives neighbours for a given bacterium

        :param= istate: *bool*
            Current bacterium state that will be evaluated

        Returns
        ^^^^^^^
            bool
        """
        if alive_neighbourgs_count < 2:
            return False

        if istate and alive_neighbourgs_count in range(2,4):
            return True

        if not istate and alive_neighbourgs_count == 3:
            return True

        if alive_neighbourgs_count > 3:
            return False

        return False

    def __init__(self, width: int, height: int) -> None:
        if width < 1 or height < 1:
            raise BadInitialGridSizeError(
                'Grid must be at least 1x1 scaled'
            )

        self.width = width
        self.height = height
        self.iterations = 0

        self.bacteria = [[ False for y in range(self.height) ] for x in range(self.width)]

    def populate_t0(self, *bacteria):
        """Populate the grid at iteration 0 (initial)

        Parameters
        ^^^^^^^^^^
        :param *bacteria: *splat*
            Alive bacteria that will populate grid for iteration 0

        Raises
        ^^^^^^
        :deadoralive.PopulateOutOfBoundsError:
            Will be raised if a bacterium x or y coordinate is negative
            Will be raised if a bacterium x or y coordinate is out of grid bounds
        """
        for bacterium in bacteria:
            if bacterium.x < 0 and bacterium.y < 0:
                raise PopulateOutOfBoundsError

            try:
                self.bacteria[bacterium.x][bacterium.y] = bacterium.alive
            except IndexError:
                raise PopulateOutOfBoundsError from IndexError

    def is_bacterium_top_left_corner(self, x: int, y: int) -> bool:
        """Check if a bacterium is in grid top-left corner

        Parameters
        ^^^^^^^^^^
        :param x: *int*
            Bacterium x coordinate
        :param y: *int*
            Bacterium y coordinate
        """

        return not x and not y

    def is_bacterium_bottom_right_corner(self, x: int, y: int) -> bool:
        """Check if a bacterium is in grid bottom-right corner

        Parameters
        ^^^^^^^^^^
        :param x: *int*
            Bacterium x coordinate
        :param y: *int*
            Bacterium y coordinate
        """

        return x == self.width - 1 and y == self.height - 1

    def is_bacterium_top_right_corner(self, x: int, y: int) -> bool:
        """Check if a bacterium is in grid top-right corner

        Parameters
        ^^^^^^^^^^
        :param x: *int*
            Bacterium x coordinate
        :param y: *int*
            Bacterium y coordinate
        """

        return x == self.width - 1 and not y

    def is_bacterium_bottom_left_corner(self, x: int, y: int) -> bool:
        """Check if a bacterium is in grid bottom-left corner

        Parameters
        ^^^^^^^^^^
        :param x: *int*
            Bacterium x coordinate
        :param y: *int*
            Bacterium y coordinate
        """

        return not x and y == self.height - 1

    def is_bacterium_on_upper_border(self, x, y) -> bool:
        """Check if a bacterium is on upper border

        Parameters
        ^^^^^^^^^^
        :param x: *int*
            Bacterium x coordinate
        :param y: *int*
            Bacterium y coordinate
        """

        return x in range(1, self.width) and not y

    def is_bacterium_on_bottom_border(self, x, y) -> bool:
        """Check if a bacterium is on bottom border

        Parameters
        ^^^^^^^^^^
        :param x: *int*
            Bacterium x coordinate
        :param y: *int*
            Bacterium y coordinate
        """
        return x in range(1, self.width) and y == self.height - 1

    def is_bacterium_on_left_border(self, x, y) -> bool:
        """Check if a bacterium is on left border

        Parameters
        ^^^^^^^^^^
        :param x: *int*
            Bacterium x coordinate
        :param y: *int*
            Bacterium y coordinate
        """

        return not x and y in range(1, self.height)

    def is_bacterium_on_right_border(self, x, y) -> bool:
        """Check if a bacterium is on right border

        Parameters
        ^^^^^^^^^^
        :param x: *int*
            Bacterium x coordinate
        :param y: *int*
            Bacterium y coordinate
        """

        return x == self.width - 1 and y in range(1, self.height)

    def neighbours(self, x: int, y: int) -> List[Bacterium]:
        """Computes for a given bacterium all it's neighbours

        Parameters
        ^^^^^^^^^^
        :param x: *int*
            Bacterium x coordinate
        :param y: *int*
            Bacterium y coordinate

        Returns
        ^^^^^^^
        List of deadoralive.Bacterium objects
        """

        if self.width == 1 and self.height == 1:
            return []

        if self.is_bacterium_top_left_corner(x, y):
            return [
                Bacterium(x+1, y, self.bacteria[x+1][y]),
                Bacterium(x, y+1, self.bacteria[x][y+1]),
                Bacterium(x+1, y+1, self.bacteria[x+1][y+1]),
            ]

        if self.is_bacterium_bottom_right_corner(x, y):
            return [
                Bacterium(x-1, y, self.bacteria[x-1][y]),
                Bacterium(x, y-1, self.bacteria[x][y-1]),
                Bacterium(x-1, y-1, self.bacteria[x-1][y-1]),
            ]

        if self.is_bacterium_top_right_corner(x, y):
            return [
                Bacterium(x-1, y, self.bacteria[x-1][y]),
                Bacterium(x, y+1, self.bacteria[x][y+1]),
                Bacterium(x-1, y+1, self.bacteria[x-1][y+1]),
            ]

        if self.is_bacterium_bottom_left_corner(x, y):
            return [
                Bacterium(x+1, y, self.bacteria[x+1][y]),
                Bacterium(x, y-1, self.bacteria[x][y-1]),
                Bacterium(x+1, y-1, self.bacteria[x+1][y-1]),
            ]

        if self.is_bacterium_on_upper_border(x, y):
            return [
                Bacterium(x-1, y, self.bacteria[x-1][y]),
                Bacterium(x+1, y, self.bacteria[x+1][y]),
                Bacterium(x, y+1, self.bacteria[x][y+1]),
                Bacterium(x-1, y+1, self.bacteria[x-1][y+1]),
                Bacterium(x+1, y+1, self.bacteria[x+1][y+1]),
            ]

        if self.is_bacterium_on_bottom_border(x, y):
            return [
                Bacterium(x-1, y, self.bacteria[x-1][y]),
                Bacterium(x+1, y, self.bacteria[x+1][y]),
                Bacterium(x, y-1, self.bacteria[x][y-1]),
                Bacterium(x-1, y-1, self.bacteria[x-1][y-1]),
                Bacterium(x+1, y-1, self.bacteria[x+1][y-1]),
            ]

        if self.is_bacterium_on_left_border(x, y):
            return [
                Bacterium(x, y-1, self.bacteria[x][y-1]),
                Bacterium(x, y+1, self.bacteria[x][y+1]),
                Bacterium(x+1, y, self.bacteria[x+1][y]),
                Bacterium(x+1, y-1, self.bacteria[x+1][y-1]),
                Bacterium(x+1, y+1, self.bacteria[x+1][y+1]),
            ]

        if self.is_bacterium_on_right_border(x, y):
            return [
                Bacterium(x, y-1, self.bacteria[x][y-1]),
                Bacterium(x, y+1, self.bacteria[x][y+1]),
                Bacterium(x-1, y, self.bacteria[x-1][y]),
                Bacterium(x-1, y-1, self.bacteria[x-1][y-1]),
                Bacterium(x-1, y+1, self.bacteria[x-1][y+1]),
            ]

        # Bacterium is not on edges, it has 8 neighbours
        return [
            Bacterium(x, y-1, self.bacteria[x][y-1]),
            Bacterium(x+1, y-1, self.bacteria[x+1][y-1]),
            Bacterium(x+1, y, self.bacteria[x+1][y]),
            Bacterium(x+1, y+1, self.bacteria[x+1][y+1]),
            Bacterium(x, y+1, self.bacteria[x][y+1]),
            Bacterium(x-1, y+1, self.bacteria[x-1][y+1]),
            Bacterium(x-1, y, self.bacteria[x-1][y]),
            Bacterium(x-1, y-1, self.bacteria[x-1][y-1]),
        ]

    def do_iteration(self):
        """Computes an iteration then update bacteria holder

        The 2D list container will be deep-copied to ensure
        evaluating each bacterium with their neighbours on the
        last iteration state.
        """

        next_bacteria_iteration = [[ False for y in range(self.height) ] for x in range(self.width)]

        for y in range(self.height):
            for x in range(self.width):
                neighbours = self.neighbours(x, y)
                alive_neighbours = len([bacterium for bacterium in neighbours if bacterium.alive])
                next_bacteria_iteration[x][y] = Grid.evaluate_next(
                    alive_neighbours, istate=self.bacteria[x][y]
                )

        self.bacteria = copy.deepcopy(next_bacteria_iteration)

        self.iterations += 1

    def __str__(self) -> str:
        display_str = f"iteration {self.iterations}\n"
        display_str += '+' + '-' * (self.width * 2 - 1) + '+\n'
        for y in range(self.height):
            display_str += '|'

            for x in range(self.width):
                display_str += 'O' if self.bacteria[x][y] else 'X'
                display_str += ' ' if x + 1 < self.width else ''

            display_str += '|\n'

        display_str += '+' + '-' * (self.width * 2 - 1) + '+\n'
        return display_str
