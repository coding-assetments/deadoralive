# SPDX-License-Identifier: 0BSD

"""Bacterium model related features module"""

from __future__ import annotations

class Bacterium:
    """Bacterium class represents the simulation bacterium unit

    Attributes
    ^^^^^^^^^^
    :attr x: *int*
        Bacterium X position in the grid
    :attr y: *int*
        Bacterium Y position in the grid
    :attr alive: *bool*
        Bacterium current state (Alive or Dead)
    """

    def __init__(self, x: int, y: int, alive: bool = True) -> None:
        self.x = x
        self.y = y
        self.alive = alive

    def __repr__(self) -> str:
        return f"\
        {self.__class__.__module__}.{self.__class__.__qualname__}(\
            x: {self.x}, y: {self.y}, alive: {self.alive}\
            )"
