# SPDX-License-Identifier: 0BSD

"""Program entrypoint"""

from deadoralive.cli import (
    handle_cli_arguments,
    parse_input_data,
    build_simulation_grid,
    run_simulation,
)

# Parse program arguments
args_parser = handle_cli_arguments()
args = args_parser.parse_args()

# Parse input data
doa_parser = parse_input_data(args)

# Build grid
simulation_grid = build_simulation_grid(doa_parser)

# Run simulation
iterations = int(doa_parser.data['iterations'])
run_simulation(simulation_grid, iterations)
