# SPDX-License-Identifier: 0BSD

"""Grid model related features module"""

from __future__ import annotations

from deadoralive.bacterium import Bacterium

class EmptyInputDataError(ValueError):
    """Empty input data related exception

    Inherits from builtin ValueError exception
    """

class BadInputDataError(ValueError):
    """Bad data format related exception

    Inherits from builtin ValueError exception
    """

class Parser:
    """Simulation input data parser class

    Attributes
    ^^^^^^^^^^
    :attr data: *dict*
        Parsed data
    """
    def __init__(self) -> None:
        self.data = {}
        self.data['bacteria'] = []

    def parse(self, input_data: str) -> None:
        """Parses a string that contains input data

        String format must be such as following:

        grid_width grid_height iteration
        bacterium_A_x bacterium_A_y
        ...
        bacterium_N_x bacterium_N_y

        Parameters
        ^^^^^^^^^^
        :param input_data: *str*
            Input string that contains data to parse
        """

        if not input_data:
            raise EmptyInputDataError

        lines = input_data.splitlines()

        metadata = lines[0].split()

        try:
            self.data['grid_width'] = int(metadata[0])
            self.data['grid_height'] = int(metadata[1])
            self.data['iterations'] = int(metadata[2])
        except ValueError as error:
            raise BadInputDataError(
                'Metadata must match "X Y iteration" format'
            ) from error

        for idx, data in enumerate(lines[1:]):
            if not data:
                continue

            coordinates = data.split()
            try:
                self.data['bacteria'].append(
                    Bacterium(
                        int(coordinates[0]),
                        int(coordinates[1]),
                        alive=True
                    )
                )
            except (IndexError, ValueError) as error:
                raise BadInputDataError(
                    # Metadata line + zero idx
                    f"Bad coordinates format at line {idx + 2}: {repr(data)}"
                ) from error

    def load(self, filename):
        """Load data from a file then parses it

        File data format must be such as following:

        grid_width grid_height iteration
        bacterium_A_x bacterium_A_y
        ...
        bacterium_N_x bacterium_N_y

        Parameters
        ^^^^^^^^^^
        :param filename: *str*
            Filepath to file that contains data to load and parse
        """
        with open(filename, 'r', encoding='utf-8') as file:
            read_data = file.read()
            self.parse(read_data)
