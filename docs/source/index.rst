.. DeadOrAlive documentation master file, created by
   sphinx-quickstart on Tue Jul 19 01:54:40 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

DeadOrAlive bacteria life simulatior
====================================

This documentation walks technical aspects of DeadOrAlive software.

For the usage documentation, see `this page <https://gitlab.com/coding-assetments/deadoralive>`_

.. toctree::
   :maxdepth: 2

   modules.rst
   deadoralive.rst
