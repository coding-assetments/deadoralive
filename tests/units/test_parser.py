# SPDX-License-Identifier: 0BSD

"""
===================
Parser unit testing module
===================
Parser features related units tests
"""

import pytest

from deadoralive.parser import Parser, EmptyInputDataError, BadInputDataError

# Fixtures
@pytest.fixture
def parser():
    return Parser()

@pytest.fixture
def valid_data():
    return \
'''3 4 3
0 0
1 0
2 0'''

@pytest.fixture
def invalid_metadata():
    return \
'''Hello world!
0 0
1 0
2 0'''

@pytest.fixture
def invalid_data():
    return \
'''3 4 3
0 0
5
A B
'''

@pytest.fixture
def whitespaced_data():
    return \
'''3            4                               3
0       0                          
    1       0        
2               0     



'''

# Unit tests
def test_parser_can_parses_string(parser, valid_data):
    parser.parse(valid_data)

    assert isinstance(parser.data, dict)

def test_parser_parses_not_empty_string(parser):
    with pytest.raises(EmptyInputDataError):
        parser.parse('')

def test_parser_parses_metadata_from_string(parser, valid_data):
    parser.parse(valid_data)

    assert parser.data['grid_width'] == 3
    assert parser.data['grid_height'] == 4
    assert parser.data['iterations'] == 3

def test_parser_parses_bacteria_from_string(parser, valid_data):
    parser.parse(valid_data)

    assert isinstance(parser.data['bacteria'], list)
    assert len(parser.data['bacteria']) == 3
    
    assert parser.data['bacteria'][0].x == 0
    assert parser.data['bacteria'][0].y == 0
    assert parser.data['bacteria'][0].alive

    assert parser.data['bacteria'][1].x == 1
    assert parser.data['bacteria'][1].y == 0
    assert parser.data['bacteria'][1].alive

    assert parser.data['bacteria'][2].x == 2
    assert parser.data['bacteria'][2].y == 0
    assert parser.data['bacteria'][2].alive

def test_parser_parses_correct_metadata(parser, invalid_metadata):
    with pytest.raises(BadInputDataError):
        parser.parse(invalid_metadata)

def test_parser_parses_correct_data(parser, invalid_data):
    with pytest.raises(BadInputDataError):
        parser.parse(invalid_data)

def test_parser_parses_stripped_metadata(parser, whitespaced_data):
    parser.parse(whitespaced_data)

    assert len(parser.data['bacteria']) == 3
    
    assert parser.data['bacteria'][0].x == 0
    assert parser.data['bacteria'][0].y == 0
    assert parser.data['bacteria'][0].alive

    assert parser.data['bacteria'][1].x == 1
    assert parser.data['bacteria'][1].y == 0
    assert parser.data['bacteria'][1].alive

    assert parser.data['bacteria'][2].x == 2
    assert parser.data['bacteria'][2].y == 0
    assert parser.data['bacteria'][2].alive
