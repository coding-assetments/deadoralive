# SPDX-License-Identifier: 0BSD

import pytest

from deadoralive.bacterium import Bacterium

@pytest.fixture
def bacterium():
    return Bacterium(1, 2)

def test_init_bacterium_with_good_coordinates(bacterium):
    assert bacterium.x == 1
    assert bacterium.y == 2

def test_init_bacterium_is_alive(bacterium):
    assert bacterium.alive is True

def test_init_bacterium_is_dead():
    assert Bacterium(1, 2, alive = False).alive is False

def test_class_repr(bacterium):
    assert repr(bacterium) == f"\
        {bacterium.__class__.__module__}.{bacterium.__class__.__qualname__}(\
            x: {bacterium.x}, y: {bacterium.y}, alive: {bacterium.alive}\
            )"