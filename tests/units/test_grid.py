# SPDX-License-Identifier: 0BSD

"""
===================
Grid unit testing module
===================
Grid features related units tests
"""

from itertools import chain
import json
import pytest

from deadoralive.bacterium import Bacterium
from deadoralive.grid import Grid, BadInitialGridSizeError, PopulateOutOfBoundsError

# Fixtures
@pytest.fixture
def grid():
    return Grid(4, 4)

@pytest.fixture
def t0_population():
    return [
        Bacterium(0, 0),
        Bacterium(1, 0),
        Bacterium(3, 0),
        Bacterium(1, 1),
        Bacterium(0, 2),
        Bacterium(2, 2),
        Bacterium(3, 2),
        Bacterium(1, 3),
    ]

# Unit tests
def test_grid_init_with_right_size(grid):
    assert grid.width == 4
    assert grid.height == 4

def test_grid_init_with_null_scale():
    with pytest.raises(BadInitialGridSizeError):
        Grid(0, 0)

def test_grid_init_with_negative_scale():
    with pytest.raises(BadInitialGridSizeError):
        Grid(-1, -1)

def test_grid_can_stores_bacteria(grid):
    assert isinstance(grid.bacteria, list)

def test_grid_bacteria_collection_matches_size(grid):
    assert len(grid.bacteria) == grid.height
    assert sum( [len(row) for row in grid.bacteria] ) == grid.width * grid.height

def test_grid_bacteria_are_all_dead_initialized(grid):
    assert sum( list(chain.from_iterable(grid.bacteria)) ) == 0

def test_grid_populate_t0_with_a_bacterium(grid):
    grid.populate_t0(Bacterium(0, 0))

    assert grid.bacteria[0][0] == 1

def test_grid_populate_t0_with_many_bacteria(grid, t0_population):
    grid.populate_t0(*t0_population)

    assert grid.bacteria[0][0] == 1
    assert grid.bacteria[1][0] == 1
    assert grid.bacteria[3][0] == 1
    assert grid.bacteria[1][1] == 1
    assert grid.bacteria[0][2] == 1
    assert grid.bacteria[2][2] == 1
    assert grid.bacteria[3][2] == 1
    assert grid.bacteria[1][3] == 1

def test_grid_populate_t0_check_for_bounds(grid):
    with pytest.raises(PopulateOutOfBoundsError):
        grid.populate_t0(Bacterium(4, 4))

    with pytest.raises(PopulateOutOfBoundsError):
        grid.populate_t0(Bacterium(-1, -1))

def test_grid_get_top_left_corner_neighbours(grid):
    grid.populate_t0(
        Bacterium(0, 0),
        Bacterium(1, 1),
    )

    neighbours = grid.neighbours(0, 0)

    assert len(neighbours) == 3

    output = json.dumps( [vars(b) for b in neighbours] )
    expected = json.dumps(
        [
            {'x': 1, 'y': 0, 'alive': False},
            {'x': 0, 'y': 1, 'alive': False},
            {'x': 1, 'y': 1, 'alive': True},
        ]
    )

    assert expected == output

def test_grid_get_bottom_right_corner_neighbours(grid):
    grid.populate_t0(
        Bacterium(3, 3),
        Bacterium(2, 2),
    )

    neighbours = grid.neighbours(3, 3)

    assert len(neighbours) == 3
    
    output = json.dumps( [vars(b) for b in neighbours] )
    expected = json.dumps(
        [
            {'x': 2, 'y': 3, 'alive': False},
            {'x': 3, 'y': 2, 'alive': False},
            {'x': 2, 'y': 2, 'alive': True},
        ]
    )

    assert expected == output

def test_grid_get_top_right_corner_neighbours(grid):
    grid.populate_t0(
        Bacterium(3, 0),
        Bacterium(2, 1),
    )

    neighbours = grid.neighbours(3, 0)

    assert len(neighbours) == 3

    output = json.dumps( [vars(b) for b in neighbours] )
    expected = json.dumps(
        [
            {'x': 2, 'y': 0, 'alive': False},
            {'x': 3, 'y': 1, 'alive': False},
            {'x': 2, 'y': 1, 'alive': True},
        ]
    )

    assert expected == output

def test_grid_get_bottom_left_corner_neighbours(grid):
    grid.populate_t0(
        Bacterium(0, 3),
        Bacterium(1, 2),
    )

    neighbours = grid.neighbours(0, 3)

    assert len(neighbours) == 3

    output = json.dumps( [vars(b) for b in neighbours] )
    expected = json.dumps(
        [
            {'x': 1, 'y': 3, 'alive': False},
            {'x': 0, 'y': 2, 'alive': False},
            {'x': 1, 'y': 2, 'alive': True},
        ]
    )

    assert expected == output

def test_grid_get_upper_border_neighbours(grid):
    grid.populate_t0(
        Bacterium(1, 0),
        Bacterium(0, 1),
        Bacterium(2, 1),
    )

    neighbours = grid.neighbours(1, 0)

    assert len(neighbours) == 5

    output = json.dumps( [vars(b) for b in neighbours] )
    expected = json.dumps(
        [
            {'x': 0, 'y': 0, 'alive': False},
            {'x': 2, 'y': 0, 'alive': False},
            {'x': 1, 'y': 1, 'alive': False},
            {'x': 0, 'y': 1, 'alive': True},
            {'x': 2, 'y': 1, 'alive': True},
        ]
    )

    assert expected == output

def test_grid_get_bottom_border_neighbours(grid):
    grid.populate_t0(
        Bacterium(1, 3),
        Bacterium(0, 2),
        Bacterium(2, 2),
    )

    neighbours = grid.neighbours(1, 3)

    assert len(neighbours) == 5

    output = json.dumps( [vars(b) for b in neighbours] )
    expected = json.dumps(
        [
            {'x': 0, 'y': 3, 'alive': False},
            {'x': 2, 'y': 3, 'alive': False},
            {'x': 1, 'y': 2, 'alive': False},
            {'x': 0, 'y': 2, 'alive': True},
            {'x': 2, 'y': 2, 'alive': True},
        ]
    )

    assert expected == output

def test_grid_get_left_border_neighbours(grid):
    grid.populate_t0(
        Bacterium(0, 2),
        Bacterium(1, 1),
        Bacterium(1, 3),
    )

    neighbours = grid.neighbours(0, 2)

    assert len(neighbours) == 5

    output = json.dumps( [vars(b) for b in neighbours] )
    expected = json.dumps(
        [
            {'x': 0, 'y': 1, 'alive': False},
            {'x': 0, 'y': 3, 'alive': False},
            {'x': 1, 'y': 2, 'alive': False},
            {'x': 1, 'y': 1, 'alive': True},
            {'x': 1, 'y': 3, 'alive': True},
        ]
    )

    assert expected == output

def test_grid_get_right_border_neighbours(grid):
    grid.populate_t0(
        Bacterium(3, 2),
        Bacterium(2, 1),
        Bacterium(2, 3),
    )

    neighbours = grid.neighbours(3, 2)
    assert len(neighbours) == 5

    output = json.dumps( [vars(b) for b in neighbours] )
    expected = json.dumps(
        [
            {'x': 3, 'y': 1, 'alive': False},
            {'x': 3, 'y': 3, 'alive': False},
            {'x': 2, 'y': 2, 'alive': False},
            {'x': 2, 'y': 1, 'alive': True},
            {'x': 2, 'y': 3, 'alive': True},
        ]
    )

    assert expected == output

def test_grid_get_middle_neighbours(grid):
    grid.populate_t0(
        Bacterium(2, 1),
        Bacterium(1, 0),
        Bacterium(3, 0),
        Bacterium(3, 2),
        Bacterium(1, 2),
    )

    neighbours = grid.neighbours(2, 1)
    assert len(neighbours) == 8

    output = json.dumps( [vars(b) for b in neighbours] )
    expected = json.dumps(
        [
            {'x': 2, 'y': 0, 'alive': False},
            {'x': 3, 'y': 0, 'alive': True},
            {'x': 3, 'y': 1, 'alive': False},
            {'x': 3, 'y': 2, 'alive': True},
            {'x': 2, 'y': 2, 'alive': False},
            {'x': 1, 'y': 2, 'alive': True},
            {'x': 1, 'y': 1, 'alive': False},
            {'x': 1, 'y': 0, 'alive': True},
        ]
    )

    assert expected == output

def test_grid_do_iteration(grid, t0_population):
    grid.populate_t0(*t0_population)

    grid.do_iteration()

    assert grid.bacteria[0][0] == True
    assert grid.bacteria[1][0] == True
    assert grid.bacteria[2][0] == True
    assert grid.bacteria[3][0] == False

    assert grid.bacteria[0][1] == False
    assert grid.bacteria[1][1] == False
    assert grid.bacteria[2][1] == False
    assert grid.bacteria[3][1] == True

    assert grid.bacteria[0][2] == True
    assert grid.bacteria[1][2] == False
    assert grid.bacteria[2][2] == True
    assert grid.bacteria[3][2] == False

    assert grid.bacteria[0][3] == False
    assert grid.bacteria[1][3] == True
    assert grid.bacteria[2][3] == True
    assert grid.bacteria[3][3] == False

def test_grid_count_iterations(grid, t0_population):
    grid.populate_t0(*t0_population)
    grid.do_iteration()
    grid.do_iteration()

    assert grid.iterations == 2
