# SPDX-License-Identifier: 0BSD

"""
=========================
Grid rules testing module
=========================
Grid iteration rules related units tests
"""

from deadoralive.grid import Grid

def test_grid_rule_lt_2_neighbourgs():
    assert Grid.evaluate_next(1) == False

def test_grid_rule_eq_2_or_eq_3_neighbourgs():
    assert Grid.evaluate_next(2) == True
    assert Grid.evaluate_next(3) == True

def test_grid_rule_gt_3_neighbourgs():
    assert Grid.evaluate_next(4) == False

def test_grid_rule_evaluate_cases_for_alive_bacterium():
    assert Grid.evaluate_next(1, istate = True) == False
    assert Grid.evaluate_next(2, istate = True) == True
    assert Grid.evaluate_next(3, istate = True) == True
    assert Grid.evaluate_next(4, istate = True) == False

def test_grid_rule_evaluate_cases_for_dead_bacterium():
    assert Grid.evaluate_next(1, istate = False) == False
    assert Grid.evaluate_next(3, istate = False) == True
    assert Grid.evaluate_next(4, istate = False) == False
