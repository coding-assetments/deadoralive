# SPDX-License-Identifier: 0BSD

"""
===================
Grid integration testing module
===================
Grid features related integrations tests
"""

from deadoralive.bacterium import Bacterium
from deadoralive.grid import Grid


def test_grid_can_be_displayed():
    grid = Grid(4, 4)
    t0_population = [
        Bacterium(0, 0),
        Bacterium(1, 0),
        Bacterium(3, 0),
        Bacterium(1, 1),
        Bacterium(0, 2),
        Bacterium(2, 2),
        Bacterium(3, 2),
        Bacterium(1, 3),
    ]

    grid.populate_t0(*t0_population)
    grid.do_iteration()

    assert grid.__str__() == \
f'''iteration 1
+-------+
|O O O X|
|X X X O|
|O X O X|
|X O O X|
+-------+
'''

def test_grid_1x1():
    grid = Grid(1, 1)
    
    grid.populate_t0(
        Bacterium(0, 0)
    )

    grid.do_iteration()

    assert grid.__str__() == \
f'''iteration 1
+-+
|X|
+-+
'''

def test_grid_2x2():
    grid = Grid(2, 2)

    grid.populate_t0(
        Bacterium(0, 0),
        Bacterium(1, 1),
    )

    grid.do_iteration()

    assert grid.__str__() == \
f'''iteration 1
+---+
|X X|
|X X|
+---+
'''

def test_grid_vertical_rectangle():
    grid = Grid(3, 4)

    grid.populate_t0(
        Bacterium(0, 0),
        Bacterium(1, 0),
        Bacterium(2, 0),
    )

    grid.do_iteration()

    assert grid.__str__() == \
f'''iteration 1
+-----+
|X O X|
|X O X|
|X X X|
|X X X|
+-----+
'''

def test_grid_horizontal_rectangle():
    grid = Grid(4, 3)

    grid.populate_t0(
        Bacterium(0, 0),
        Bacterium(0, 1),
        Bacterium(0, 2)
    )

    grid.do_iteration()

    assert grid.__str__() == \
f'''iteration 1
+-------+
|X X X X|
|O O X X|
|X X X X|
+-------+
'''
