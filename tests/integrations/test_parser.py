# SPDX-License-Identifier: 0BSD

"""
===================
Parser integration testing module
===================
Parser features related integration tests
"""

import pytest

from deadoralive.parser import Parser

# Fixtures
@pytest.fixture
def mocker_data_file(mocker):
    mocked_data_file = mocker.mock_open(
        read_data = '''3 4 3
        0 0
        1 0
        2 0'''
    )

    mocker.patch('builtins.open', mocked_data_file)

# Integration tests
def test_parser_load_and_parse_file(mocker_data_file):
    parser = Parser()

    parser.load('fakefile')

    assert len(parser.data['bacteria']) == 3
    
    assert parser.data['bacteria'][0].x == 0
    assert parser.data['bacteria'][0].y == 0
    assert parser.data['bacteria'][0].alive

    assert parser.data['bacteria'][1].x == 1
    assert parser.data['bacteria'][1].y == 0
    assert parser.data['bacteria'][1].alive

    assert parser.data['bacteria'][2].x == 2
    assert parser.data['bacteria'][2].y == 0
    assert parser.data['bacteria'][2].alive
